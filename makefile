all: ed.86

edd.exe: ed.86
	udi2dos ed edd

ed.86: ed.obj scp055.obj scp173.obj scp682.obj
	link86 ed.obj,scp055.obj,scp173.obj,scp682.obj to ed.86 bind segsize(memory(2000h))

scp055.obj: scp055.a86
	asm86 scp055.a86 debug symbols

scp173.obj: scp173.a86
	asm86 scp173.a86 debug symbols

ed.obj: ed.plm
	plm86 ed.plm xref optimize(0) debug symbols

scp682.obj: scp682.plm
	plm86 scp682.plm xref optimize(0) debug symbols

clean:  .symbolic
	del *.86
	del *.bak
	del *.bkp
	del *.lst
	del *.mp1
	del *.obj
	del *.exe

