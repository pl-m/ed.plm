# ed.plm

The primary goal of this project is to adapt the Concurrent CP/M-86 text editor and make it usable on other plataforms.  First on DOS, then on different kinds of CP/M, and then other operating system having a PL/M compiler.

Another goal is to document and to understand the internals of the application.

A third goal is to adapt the application to make it easier to use.
